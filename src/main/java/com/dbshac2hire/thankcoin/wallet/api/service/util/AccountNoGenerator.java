package com.dbshac2hire.thankcoin.wallet.api.service.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

/**
 *
 * @author isak.rabin
 */
public class AccountNoGenerator {
    
    public static String generate() throws NoSuchAlgorithmException {
        String uuid = UUID.randomUUID().toString();

        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(uuid.getBytes(), 0, uuid.length());

        return String.format("%032x", new BigInteger(1,md.digest()));
    }

}
