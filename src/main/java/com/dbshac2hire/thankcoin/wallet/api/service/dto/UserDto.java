package com.dbshac2hire.thankcoin.wallet.api.service.dto;

/**
 *
 * @author isak.rabin
 */
public class UserDto {

    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
