package com.dbshac2hire.thankcoin.wallet.api.controller;

import com.dbshac2hire.thankcoin.wallet.api.domain.User;
import com.dbshac2hire.thankcoin.wallet.api.service.dto.AccountDto;
import com.dbshac2hire.thankcoin.wallet.api.service.dto.TransactionDto;
import com.dbshac2hire.thankcoin.wallet.api.service.vo.AccountVo;
import com.dbshac2hire.thankcoin.wallet.api.service.vo.TransactionVo;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author isak.rabin
 */
@RestController
public class AccountController extends BaseController {

    @RequestMapping(path = "/accounts/create", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<AccountVo> create(HttpServletRequest request, @RequestBody AccountDto account) throws NoSuchAlgorithmException, Exception {
        AccountVo response = new AccountVo();

        String authorizationKey = request.getHeader("Authorization");
        if (StringUtils.isEmpty(authorizationKey)) {
            response.setMessage("Unauthorized Access Detected");
            return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
        }

        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(setting.getAuthKey()))
                .parseClaimsJws(authorizationKey).getBody();
        String email = claims.getSubject();
        String id = claims.getId();

        User user;
        try {
            user = userService.findUserByIdAndEmail(Long.valueOf(id), email);
            if (user == null) {
                response.setMessage("Unauthorized Access Detected");
                return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
            }
        } catch (Exception ex) {
            response.setMessage("Unauthorized Access Detected");
            return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
        }

        if (account.getAmount() < 0 || account.getAmount() > Long.MAX_VALUE) {
            response.setMessage("Invalid amount to create account");
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        AccountVo accountVo = accountService.donate(user.getId(), account.getAmount(), account.getDescription());
        return new ResponseEntity(accountVo, HttpStatus.OK);
    }

    @RequestMapping(path = "/accounts/{accountNo}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<AccountVo> checkBalance(HttpServletRequest request, @PathVariable String accountNo, @RequestParam(defaultValue = "false", required = false) Boolean open) throws Exception {
        AccountVo response = new AccountVo();

        String authorizationKey = request.getHeader("Authorization");
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(setting.getAuthKey()))
                .parseClaimsJws(authorizationKey).getBody();
        String email = claims.getSubject();
        String id = claims.getId();

        User user;
        try {
            user = userService.findUserByIdAndEmail(Long.valueOf(id), email);
            if (user == null) {
                response.setMessage("Unauthorized Access Detected");
                return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
            }
        } catch (Exception ex) {
            response.setMessage("Unauthorized Access Detected");
            return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
        }

        if (!open) {
            AccountVo accountVo = accountService.getAccount(user.getId());
            if (!accountNo.equalsIgnoreCase(accountVo.getNumber())) {
                response.setMessage("Unauthorized Access Detected");
                return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
            }
        }

        response = accountService.getAccount(accountNo);
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @RequestMapping(path = "/accounts/{accountNo}/transfer", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<AccountVo> transfer(HttpServletRequest request, @PathVariable String accountNo, @RequestBody TransactionDto transaction) throws Exception {
        AccountVo response = new AccountVo();

        String authorizationKey = request.getHeader("Authorization");
        if (StringUtils.isEmpty(authorizationKey)) {
            response.setMessage("Unauthorized Access Detected");
            return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
        }

        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(setting.getAuthKey()))
                .parseClaimsJws(authorizationKey).getBody();
        String email = claims.getSubject();
        String id = claims.getId();

        User user;
        try {
            user = userService.findUserByIdAndEmail(Long.valueOf(id), email);
            if (user == null) {
                response.setMessage("Unauthorized Access Detected");
                return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
            }
        } catch (Exception ex) {
            response.setMessage("Unauthorized Access Detected");
            return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
        }

        AccountVo accountVo = accountService.getAccount(user.getId());
        if (!accountNo.equalsIgnoreCase(accountVo.getNumber())) {
            response.setMessage("Unauthorized Access Detected");
            return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
        }

        if (StringUtils.isEmpty(transaction.getAccountTo()) && StringUtils.isEmpty(transaction.getEmailTo())) {
            response.setMessage("Bad Request");
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        accountVo = accountService.transfer(user.getAccountNo(), transaction.getEmailTo(), transaction.getAccountTo(), transaction.getAmount(), transaction.getDescription(), transaction.getSignature());

        return new ResponseEntity(accountVo, HttpStatus.OK);
    }

    @RequestMapping(path = "/accounts/{accountNo}/receive", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<AccountVo> receive(HttpServletRequest request, @PathVariable String accountNo, @RequestBody TransactionDto transaction) throws Exception {
        AccountVo response = new AccountVo();

        String authorizationKey = request.getHeader("Authorization");
        if (StringUtils.isEmpty(authorizationKey)) {
            response.setMessage("Unauthorized Access Detected");
            return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
        }

        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(setting.getAuthKey()))
                .parseClaimsJws(authorizationKey).getBody();
        String email = claims.getSubject();
        String id = claims.getId();

        User user;
        try {
            user = userService.findUserByIdAndEmail(Long.valueOf(id), email);
            if (user == null) {
                response.setMessage("Unauthorized Access Detected");
                return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
            }
        } catch (Exception ex) {
            response.setMessage("Unauthorized Access Detected");
            return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
        }

        AccountVo accountVo = accountService.getAccount(user.getId());
        if (!accountNo.equalsIgnoreCase(accountVo.getNumber())) {
            response.setMessage("Unauthorized Access Detected");
            return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
        }

        AccountVo openAccountVo = accountService.getAccount(transaction.getAccountFrom());
        if (!openAccountVo.isOpen()) {
            response.setMessage("Sender is not open account");
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        accountService.transfer(transaction.getAccountFrom(), user.getEmail(), user.getAccountNo(), transaction.getAmount(), transaction.getDescription(), transaction.getSignature());
        accountVo = accountService.getAccount(user.getId());

        return new ResponseEntity(accountVo, HttpStatus.OK);
    }

    @RequestMapping(path = "/accounts/{accountNo}/history", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> history(HttpServletRequest request, @PathVariable String accountNo) throws Exception {
        AccountVo response = new AccountVo();

        String authorizationKey = request.getHeader("Authorization");
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(setting.getAuthKey()))
                .parseClaimsJws(authorizationKey).getBody();
        String email = claims.getSubject();
        String id = claims.getId();

        User user;
        try {
            user = userService.findUserByIdAndEmail(Long.valueOf(id), email);
            if (user == null) {
                response.setMessage("Unauthorized Access Detected");
                return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
            }
        } catch (Exception ex) {
            response.setMessage("Unauthorized Access Detected");
            return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
        }

        AccountVo accountVo = accountService.getAccount(user.getId());
        if (!accountNo.equalsIgnoreCase(accountVo.getNumber())) {
            response.setMessage("Unauthorized Access Detected");
            return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
        }

        List<TransactionVo> transactionList = accountService.history(accountNo);
        return new ResponseEntity(transactionList, HttpStatus.OK);
    }

}
