package com.dbshac2hire.thankcoin.wallet.api.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author isak.rabin
 */
@Entity
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String email;
    private String password;
    private String accountNo;
    private Date registerTime;

    public User() {
        this.registerTime = new Date();
    }

    public User(String email, String password) {
        this();
        this.email = email;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public Date getRegisterTime() {
        return new Date(registerTime.getTime());
    }

    public void setRegisterTime(Date registerTime) {
        if (registerTime == null) {
            this.registerTime = new Date(registerTime.getTime());
        } else {
            this.registerTime.setTime(registerTime.getTime());
        }
    }

}
