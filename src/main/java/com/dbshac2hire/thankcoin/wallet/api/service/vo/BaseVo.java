package com.dbshac2hire.thankcoin.wallet.api.service.vo;

/**
 *
 * @author isak.rabin
 */
public abstract class BaseVo {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
