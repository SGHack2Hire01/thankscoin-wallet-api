package com.dbshac2hire.thankcoin.wallet.api.service.dto;

/**
 *
 * @author isak.rabin
 */
public class AccountDto {

    private String number;
    private boolean open;
    private Long amount;
    private String description;

    public String getNumber() {
        return number;
    }

    public void setNumber(String accountNo) {
        this.number = accountNo;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
