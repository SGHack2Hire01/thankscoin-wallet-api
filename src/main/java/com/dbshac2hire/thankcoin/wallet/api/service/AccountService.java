package com.dbshac2hire.thankcoin.wallet.api.service;

import com.dbshac2hire.thankcoin.wallet.api.domain.User;
import com.dbshac2hire.thankcoin.wallet.api.repository.UserRepository;
import com.dbshac2hire.thankcoin.wallet.api.service.dto.AccountDto;
import com.dbshac2hire.thankcoin.wallet.api.service.dto.TransactionDto;
import com.dbshac2hire.thankcoin.wallet.api.service.util.AccountNoGenerator;
import com.dbshac2hire.thankcoin.wallet.api.service.vo.AccountVo;
import com.dbshac2hire.thankcoin.wallet.api.service.vo.TransactionVo;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 *
 * @author isak.rabin
 */
@Service
public class AccountService {

    private final String LEDGER_ENDPOINT = "http://localhost:8080/";

    @Autowired
    private RestClientService restClient;

    @Autowired
    private UserRepository userRepository;

    @Transactional
    public AccountVo init(Long userId) throws NoSuchAlgorithmException, Exception {

        AccountDto account = new AccountDto();
        account.setNumber(AccountNoGenerator.generate());

        String createAccountURL = LEDGER_ENDPOINT + "/account";
        restClient.getRestTemplate().postForEntity(createAccountURL, account, AccountVo.class);

        Optional<User> result = userRepository.findById(userId);
        if (!result.isPresent()) {
            throw new Exception("Unauthorize access detected");
        }

        User entity = result.get();
        entity.setId(userId);
        entity.setAccountNo(account.getNumber());
        userRepository.save(entity);

        TransactionDto transaction = new TransactionDto();
        transaction.setAccountFrom("corporate");
        transaction.setAccountTo(account.getNumber());
        transaction.setAmount(1000L);
        transaction.setDescription("Welcome Coins");
        transaction.setSignature("");

        String transferAccountURL = LEDGER_ENDPOINT + "/transaction/";
        restClient.getRestTemplate().postForEntity(transferAccountURL, transaction, Object.class);

        String retrieveAccountURL = LEDGER_ENDPOINT + "/account/" + account.getNumber();
        AccountVo userAccount = restClient.getRestTemplate().getForObject(retrieveAccountURL, AccountVo.class);

        return userAccount;
    }

    public AccountVo donate(Long userId, Long amount, String description) throws NoSuchAlgorithmException, Exception {
        AccountVo accountVo = AccountService.this.getAccount(userId);
        if (accountVo.getBalance() < amount) {
            throw new Exception("User does not have enough balance");
        }

        AccountDto account = new AccountDto();
        account.setNumber(AccountNoGenerator.generate());
        account.setAmount(amount);

        String createAccountURL = LEDGER_ENDPOINT + "/account";
        restClient.getRestTemplate().postForEntity(createAccountURL, account, AccountVo.class);

        TransactionDto transaction = new TransactionDto();
        transaction.setAccountFrom(accountVo.getNumber());
        transaction.setAccountTo(account.getNumber());
        transaction.setAmount(amount);
        transaction.setDescription(description);
        transaction.setSignature("");

        String transferAccountURL = LEDGER_ENDPOINT + "/transaction/";
        restClient.getRestTemplate().postForEntity(transferAccountURL, transaction, Object.class);

        return AccountService.this.getAccount(userId);
    }

    public AccountVo getAccount(String accountNo) {
        String retrieveAccountURL = LEDGER_ENDPOINT + "/account/" + accountNo;
        return restClient.getRestTemplate().getForObject(retrieveAccountURL, AccountVo.class);
    }
    
    public AccountVo getAccount(Long userId) throws Exception {
        Optional<User> result = userRepository.findById(userId);
        if (!result.isPresent()) {
            throw new Exception("Unauthorize access detected");
        }

        String retrieveAccountURL = LEDGER_ENDPOINT + "/account/" + result.get().getAccountNo();
        AccountVo userAccount = restClient.getRestTemplate().getForObject(retrieveAccountURL, AccountVo.class);

        return userAccount;
    }

    public AccountVo transfer(String accountFrom, String emailTo, String accountTo, Long amount, String description, String signature) {

        if (StringUtils.isEmpty(accountTo)) {
            Optional<User> user = userRepository.findByEmail(emailTo);
            if (user.isPresent()) {
                accountTo = user.get().getAccountNo();
            }
        }

        TransactionDto transaction = new TransactionDto();
        transaction.setAccountFrom(accountFrom);
        transaction.setAccountTo(accountTo);
        transaction.setAmount(amount);
        transaction.setDescription(description);
        transaction.setSignature(signature);

        String transferAccountURL = LEDGER_ENDPOINT + "/transaction/";
        restClient.getRestTemplate().postForEntity(transferAccountURL, transaction, Object.class);

        String retrieveAccountURL = LEDGER_ENDPOINT + "/account/" + accountFrom;
        AccountVo userAccount = restClient.getRestTemplate().getForObject(retrieveAccountURL, AccountVo.class);

        return userAccount;

    }

    public List<TransactionVo> history(String accountNo) {
        String historyURL = LEDGER_ENDPOINT + "/transactionshistory?accountNumber=" + accountNo;

        AccountVo accountVo = new AccountVo();
        accountVo.setNumber(accountNo);
        
        List<TransactionVo> transactionList = restClient.getRestTemplate().getForObject(historyURL, List.class);
        return transactionList;
    }

}
