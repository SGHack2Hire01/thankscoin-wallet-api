package com.dbshac2hire.thankcoin.wallet.api.service;

import com.dbshac2hire.thankcoin.wallet.api.domain.User;
import com.dbshac2hire.thankcoin.wallet.api.repository.UserRepository;
import java.util.Date;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author isak.rabin
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public boolean isExist(String email, String password) {
        Optional<User> user = userRepository.findByEmailAndPassword(email, password);
        return user.isPresent();
    }

    @Transactional
    public User register(String email, String password) throws Exception {
        Optional<User> user = userRepository.findByEmailAndPassword(email, password);
        if (!user.isPresent()) {
            User entity = new User(email, password);
            entity.setRegisterTime(new Date());
            return userRepository.save(entity);
        }
        throw new Exception("User has registered");
    }
    
    public User findUserByEmailAndPassword(String email, String password) throws Exception {
        Optional<User> user = userRepository.findByEmailAndPassword(email, password);
        if (user.isPresent()) {
            return user.get();
        }
        throw new Exception("User not found");
    }

    public User findUserByIdAndEmail(Long id, String email) throws Exception {
        Optional<User> user = userRepository.findByIdAndEmail(id, email);
        if (user.isPresent()) {
            return user.get();
        }
        throw new Exception("User not found");
    }

    public User findUserById(Long userId) throws Exception {
        Optional<User> user = userRepository.findById(userId);
        if (user.isPresent()) {
            return user.get();
        }
        throw new Exception("User not found");
    }

}
