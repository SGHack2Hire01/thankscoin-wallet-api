package com.dbshac2hire.thankcoin.wallet.api.common;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 *
 * @author isak.rabin
 */
@ConfigurationProperties(prefix = "wallet")
@Component
public class WalletSetting {

    private String authKey;

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

}
